# askomics-commands

Helper scripts for starting, stopping and updating [AskOmics](https://github.com/askomics).

Copy or link the scripts into the same directory as `docker-compose.yml` (usually in `${ASKOMICS_HOME}/federated`).
This can be facilitated by the `install-askomics-commands` script.



# Usage

- `install-askomics-commands`: creates links to the next three commands in the `standalone` and `federated` subdirectories of AskOmics' home
    - `install-askomics-commands` (no argument): relies on the `${ASKOMICS_HOME}` environment variable to figure out where AskOmics is installed
    - `install-askomics-commands <path to AskOmics>`: same thing, but you have to provide AskOmics' location
- `askostart`: starts AskOmics. Visit http://localhost/askomics
- `askostop`: stops AskOmics
- `askoupdate`: checks whether a new version of AskOmics is available, and installs it if necessary

